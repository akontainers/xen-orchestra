## New release

New version: _5.y.z_

### Changelog

- Add container tag `xen-orchestra:5.y.z`
- Point container tags `xen-orchestra:5`, `xen-orchestra:5.y` and `xen-orchestra:latest` to `xen-orchestra:5.y.z`
- Point container tags `xen-orchestra:stable` and `xen-orchestra:5.y` to `xen-orchestra:5.y.z`
- Remove build for version `5.y.z`

/assign @AKorezin
