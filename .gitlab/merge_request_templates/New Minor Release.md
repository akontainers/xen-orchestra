## New release

New version: _5.y.0_

### Changelog

- Add container tags `xen-orchestra:5.y` and `xen-orchestra:5.y.0`
- Point container tags `xen-orchestra:5` and `xen-orchestra:latest` to `xen-orchestra:5.y.0`
- Point container tags `xen-orchestra:stable` to `xen-orchestra:5.y.z`
- Remove build for version `5.y.z`

/assign @AKorezin
